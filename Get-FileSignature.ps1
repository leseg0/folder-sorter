﻿function Get-FileSignature { 
    <#
        .SYNOPSIS
            Displays a file signature for specified file or files

        .DESCRIPTION
            Displays a file signature for specified file or files. Determined by getting the bytes of a file and
            looking at the number of bytes to return and where in the byte array to start.

        .PARAMETER Path
            The path to a file. Can be multiple files.

        .PARAMETER HexFilter
            A filter that can be used to find specific Hex Signatures. Allows "*" wildcard.

        .PARAMETER ByteLimit
            How many bytes of the file signature to return. 
            Default value is 2. (Display first 2 bytes)

        .PARAMETER ByteOffset
            Where in the byte array to start displaying the signature. 
            Default value is 0 (first byte)

        .NOTES
            Name: Get-FileSignature
            Author: Boe Prox
            Created: 3 Jan 2014
            Version History
                1.4 -- 19 Feb 2014
                    -Fixed bug where a ' (CHAR 39) in path would cause script to halt attempts at pulling signature
                1.3 -- 27 Jan 2014
                    -Fixed annoying bug with a non-existent filestream trying to be closed causing
                    everything in Process block to quit
                1.2 -- 14 Jan 2014
                    -Added HexFilter parameter
                1.1 -- 12 Jan 2014
                    -Incorporated modified version of Get-FileTimestamp function for 
                    ChangeTime attribute
                1.0 -- 3 Jan 2014
                    -Initial Version

        .OUTPUTS
            System.IO.FileInfo.Signature

        .LINK
            http://en.wikipedia.org/wiki/List_of_file_signatures

        .EXAMPLE
            Get-FileSignature -Path PSExec.Exe

            Name           : PSExec.Exe
            FullName       : C:\Users\joeuser\Desktop\PSExec.Exe
            HexSignature   : 4D5A
            ASCIISignature : MZ
            Length         : 381816

            Description
            -----------
            Displays the file signature for psexec.exe using default parameter values. In this case, 
            the signature shows the file to be an executable.

        .EXAMPLE
            Get-ChildItem | Get-FileSignature -HexFilter 4D5A | Format-Table

            Name          FullName                             HexSignature ASCIISignature  Length
            ----          --------                             ------------ --------------  ------
            handle.exe    C:\Users\joeuser\Desktop\handle.exe    4D5A         MZ              423288
            PortQry.exe   C:\Users\joeuser\Desktop\PortQry.exe   4D5A         MZ              143360
            procexp.exe   C:\Users\joeuser\Desktop\procexp.exe   4D5A         MZ             4754224
            Procmon.exe   C:\Users\joeuser\Desktop\Procmon.exe   4D5A         MZ             2474608
            Procmon64.txt C:\Users\joeuser\Desktop\Procmon64.txt 4D5A         MZ             1259376
            PsExec.exe    C:\Users\joeuser\Desktop\PsExec.exe    4D5A         MZ              381816
            pskill.exe    C:\Users\joeuser\Desktop\pskill.exe    4D5A         MZ              468592
            Tcpview.exe   C:\Users\joeuser\Desktop\Tcpview.exe   4D5A         MZ              299896

            Description
            -----------
            Returns only files which match a specified signature (4D5A) for executables"

        .EXAMPLE
            Get-FileSignature -Path Procmon64.txt

            Name           : Procmon64.txt
            FullName       : C:\Users\joeuser\Desktop\Procmon64.txt
            HexSignature   : 4D5A
            ASCIISignature : MZ
            Length         : 1259376

            Description
            -----------
            Displays the file signature for Procmon64.txt using default parameter values. In this case, 
            the signature shows the file to be an executable even though it has a .txt extension.

        .EXAMPLE
            Get-FileSignature -Path OneNote2003.iso -ByteLimit 5 -ByteOffset 0x8001

            Name           : OneNote2003.iso
            FullName       : C:\Users\joeuser\Desktop\OneNote2003.iso
            HexSignature   : 4344 3030 31
            ASCIISignature : CD001
            Length         : 89960448

            Description
            -----------
            This returns the proper file signature for an ISO file by using a specified ByteOffset of 0x8001
            and the ByteLimit of 5.

    #>
    #Requires -Version 3.0
    [CmdletBinding()]
    Param(
       [Parameter(Position=0,Mandatory=$true, ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$True)]
       [Alias("FullName")]
       [string[]]$Path,
       [parameter()]
       [Alias('Filter')]
       [string]$HexFilter = "*",
       [parameter()]
       [int]$ByteLimit = 2,
       [parameter()]
       [Alias('OffSet')]
       [int]$ByteOffset = 0
    )
    Begin {
        #Determine how many bytes to return if using the $ByteOffset
        $TotalBytes = $ByteLimit + $ByteOffset

        #Clean up filter so we can perform a regex match
        #Also remove any spaces so we can make it easier to match
        [regex]$pattern = ($HexFilter -replace '\*','.*') -replace '\s',''

        #region Helper Functions
        Write-Verbose "Loading Get-FileTimeStamp helper function"
        Function Get-FileTimeStamp {
            [cmdletbinding()]
            Param (
                [parameter(ValueFromPipeline = $True,Mandatory=$True)]
                [IO.FileStream]$FileStream
            )
            Process {
                $sig = @'
                    using System;
                    using System.Runtime.InteropServices; 
                    using Microsoft.Win32.SafeHandles; 
                    using System.Collections.Generic; 
                    using System.IO;
                    using System.Text; 
                    using System.Threading; 
                    using System.Diagnostics; 
                    public class FileTimeStamp
                    {

                        struct IO_STATUS_BLOCK
                        {
                            internal uint status;
                            internal ulong information;
                        }
                        enum FILE_INFORMATION_CLASS
                        {
                            FileDirectoryInformation = 1,        // 1
                            FileFullDirectoryInformation,        // 2
                            FileBothDirectoryInformation,        // 3
                            FileBasicInformation,            // 4
                            FileStandardInformation,        // 5
                            FileInternalInformation,        // 6
                            FileEaInformation,            // 7
                            FileAccessInformation,            // 8
                            FileNameInformation,            // 9
                            FileRenameInformation,            // 10
                            FileLinkInformation,            // 11
                            FileNamesInformation,            // 12
                            FileDispositionInformation,        // 13
                            FilePositionInformation,        // 14
                            FileFullEaInformation,            // 15
                            FileModeInformation = 16,        // 16
                            FileAlignmentInformation,        // 17
                            FileAllInformation,            // 18
                            FileAllocationInformation,        // 19
                            FileEndOfFileInformation,        // 20
                            FileAlternateNameInformation,        // 21
                            FileStreamInformation,            // 22
                            FilePipeInformation,            // 23
                            FilePipeLocalInformation,        // 24
                            FilePipeRemoteInformation,        // 25
                            FileMailslotQueryInformation,        // 26
                            FileMailslotSetInformation,        // 27
                            FileCompressionInformation,        // 28
                            FileObjectIdInformation,        // 29
                            FileCompletionInformation,        // 30
                            FileMoveClusterInformation,        // 31
                            FileQuotaInformation,            // 32
                            FileReparsePointInformation,        // 33
                            FileNetworkOpenInformation,        // 34
                            FileAttributeTagInformation,        // 35
                            FileTrackingInformation,        // 36
                            FileIdBothDirectoryInformation,        // 37
                            FileIdFullDirectoryInformation,        // 38
                            FileValidDataLengthInformation,        // 39
                            FileShortNameInformation,        // 40
                            FileHardLinkInformation = 46        // 46    
                        }
                        [StructLayout(LayoutKind.Explicit)]
                        struct FILE_BASIC_INFORMATION
                        {
                            [FieldOffset(0)]
                            internal long CreationTime;
                            [FieldOffset(8)]
                            internal long LastAccessTime;
                            [FieldOffset(16)]
                            internal long LastWriteTime;
                            [FieldOffset(24)]
                            internal long ChangeTime;
                            [FieldOffset(32)]
                            internal ulong FileAttributes;
                        }
                        [DllImport("ntdll.dll", SetLastError = true)]
                        static extern IntPtr NtQueryInformationFile(SafeFileHandle fileHandle, ref IO_STATUS_BLOCK IoStatusBlock, IntPtr pInfoBlock, uint length, FILE_INFORMATION_CLASS fileInformation);

                        public static bool GetFourFileTimes(FileStream Stream,
                                out DateTime creationTime, out DateTime lastAccessTime, out DateTime lastWriteTime, out DateTime changeTime, out string errMsg)
                        {
                            bool brc = false;
                            creationTime = default(DateTime);
                            lastAccessTime = default(DateTime);
                            lastWriteTime = default(DateTime);
                            changeTime = default(DateTime);
                            errMsg = string.Empty;
                            IntPtr p_fbi = IntPtr.Zero;
                            try
                            {
                                FILE_BASIC_INFORMATION fbi = new FILE_BASIC_INFORMATION();
                                IO_STATUS_BLOCK iosb = new IO_STATUS_BLOCK();
                                using (Stream)
                                {
                                    p_fbi = Marshal.AllocHGlobal(Marshal.SizeOf(fbi));
                                    IntPtr iprc = NtQueryInformationFile(Stream.SafeFileHandle, ref iosb, p_fbi, (uint)Marshal.SizeOf(fbi), FILE_INFORMATION_CLASS.FileBasicInformation);
                                    brc = iprc == IntPtr.Zero && iosb.status == 0;
                                    if (brc)
                                    {
                                        brc = false;
                                        fbi = (FILE_BASIC_INFORMATION)Marshal.PtrToStructure(p_fbi, typeof(FILE_BASIC_INFORMATION));
                                        creationTime = DateTime.FromFileTime(fbi.CreationTime);
                                        lastAccessTime = DateTime.FromFileTime(fbi.LastAccessTime);
                                        lastWriteTime = DateTime.FromFileTime(fbi.LastWriteTime);
                                        changeTime = DateTime.FromFileTime(fbi.ChangeTime);
                                        brc = true;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                brc = false;
                                errMsg = ex.Message;
                            }
                            finally
                            {
                                if (p_fbi != IntPtr.Zero) { Marshal.FreeHGlobal(p_fbi); }
                            }
                            return brc;
                        }
                    }
'@
                #endregion Create signature from inline C# code

                #region Create Win32 API object
                Try {
                    [void][FileTimeStamp]
                } Catch {
                    Add-Type -TypeDefinition $sig
                }
                #endregion Create Win32 API object

                #region Create reference variables
                $creationTime = (Get-Date)
                $lastAccessTime = (Get-Date)
                $lastWriteTime = (Get-Date)
                $changeTime = (Get-Date)
                $errorMsg = $null
                #endregion Create reference variables

                #region Get file timestamps
                $return = [FileTimeStamp]::GetFourFileTimes($filestream,
                    [ref]$creationTime,
                    [ref]$lastAccessTime,
                    [ref]$lastWriteTime,
                    [ref]$changeTime,
                    [ref]$errorMsg
                )
                #endregion Get file timestamps
                If ($return) {
                    [datetime]$changeTime
                } Else {Write-Warning ("{0}" -f $errorMsg)}
            }
        }
        #endregion Helper Functions
    }
    Process { 
        ForEach ($item in $Path) { 
            Write-Verbose "Item: $($Item)"
            Try {    
                Try {
                    $item = Get-Item -LiteralPath (Convert-Path -LiteralPath $item -ErrorAction Stop) -Force -ErrorAction Stop
                } Catch {
                    #Need to replace the ' with a ’ so the convert-path will not fail                
                    $item = Get-Item -LiteralPath (Convert-Path -LiteralPath ($item -replace [char]39,[char]8217)) -Force -ErrorAction Stop
                }
            } Catch {
                Write-Warning "$($item): $($_.Exception.Message)"
                Return
            }
            If (Test-Path -Path $item -Type Container) {
                Write-Warning ("Cannot find signature on directory: {0}" -f $item)
            } Else {
                Try {
                    Write-Verbose "Comparing byte length ($($item.Length)) to TotalBytes ($totalbytes)"
                    If ($Item.length -ge $TotalBytes) {
                        #Open a FileStream to the file; this will prevent other actions against file until it closes
                        $filestream = New-Object IO.FileStream($Item, [IO.FileMode]::Open, [IO.FileAccess]::Read)

                        #Determine starting point
                        [void]$filestream.Seek($ByteOffset, [IO.SeekOrigin]::Begin)

                        #Create Byte buffer to read into and then read bytes from starting point to pre-determined stopping point
                        $bytebuffer = New-Object "Byte[]" ($filestream.Length - ($filestream.Length - $ByteLimit))
                        [void]$filestream.Read($bytebuffer, 0, $bytebuffer.Length)

                        #Create string builder objects for hex and ascii display
                        $hexstringBuilder = New-Object Text.StringBuilder
                        $stringBuilder = New-Object Text.StringBuilder

                        #Begin converting bytes
                        For ($i=0;$i -lt $ByteLimit;$i++) {
                            If ($i%2) {
                                [void]$hexstringBuilder.Append(("{0:X}" -f $bytebuffer[$i]).PadLeft(2, "0"))
                            } Else {
                                If ($i -eq 0) {
                                    [void]$hexstringBuilder.Append(("{0:X}" -f $bytebuffer[$i]).PadLeft(2, "0"))
                                } Else {
                                    [void]$hexstringBuilder.Append(" ")
                                    [void]$hexstringBuilder.Append(("{0:X}" -f $bytebuffer[$i]).PadLeft(2, "0"))
                                }        
                            }
                            If ([char]::IsLetterOrDigit($bytebuffer[$i])) {
                                [void]$stringBuilder.Append([char]$bytebuffer[$i])
                            } Else {
                                [void]$stringBuilder.Append(".")
                            }
                        }
                        If (($hexstringBuilder.ToString() -replace '\s','') -match $pattern) {
                            $object = [pscustomobject]@{
                                Name = ($item -replace '.*\\(.*)','$1')
                                FullName = $item
                                HexSignature = $hexstringBuilder.ToString()
                                ASCIISignature = $stringBuilder.ToString()
                                Length = $item.length
                                Extension = $Item.fullname -replace '.*\.(.*)','$1'
                                ChangeTime = ($FileStream | Get-FileTimeStamp)
                            }
                            $object.pstypenames.insert(0,'System.IO.FileInfo.Signature')
                            Write-Output $object
                        }
                    } ElseIf ($Item.length -eq 0) {
                        ## This can cause a lot of unneeded noise in the console so I am leaving this commented out
                        #Write-Warning ("{0} has no data ({1} bytes)!" -f $item.name,$item.length)
                        Return
                    } Else {                                                
                        ## This can cause a lot of unneeded noise in the console so I am leaving this commented out
                        #Write-Warning ("{0} size ({1}) is smaller than required total bytes ({2})" -f $item.name,$item.length,$TotalBytes)
                        Return
                    }
                } Catch {
                    Write-Warning ("{0}: {1}" -f $item,$_.Exception.Message)
                    Return
                }

                #Close the file stream so the file is no longer locked by the process
                $FileStream.Close()
            }
        }        
    }
}