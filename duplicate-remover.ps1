﻿$logFile = "duplicate-remover-log.txt"

Function Log {
    param(
        [Parameter(Mandatory=$true)][String]$msg
    )
    Add-Content $logFile $msg   
}

$a = Get-ChildItem -Recurse | Get-FileHash | Group-Object Hash | ? {$_.Count -gt 1}
foreach($dup in $a){
    $b = $dup.Group | Sort-Object -Property Path -Descending
    Log "Keeping: $($b[0].Path)" 
    for($i = 1; $i -lt $b.Count;$i++){
        $fileToDelete = $b[$i].Path
        try{
            Remove-Item $b[$i].Path
            Log "Deleted: $($fileToDelete)" 
        }catch{
            Log "There was an error!" 
        }
    }
    Log " "
}