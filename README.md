# Organiser Scripts

## Folder Sorter

This script are used to sort files in a directory by iteratively going through each folder and file, then copying it over to a folder based on its file type.
This initially started as a script to sort photos but has been expanding to sort more file types.
The sorting of each is outlined below:

- Pictures
	- Sorts by Year then Month. This depends on the date stored within an image.
- Audio Files
	- Currently identifies WAV and MP3 file types
- Documents
	- Currently identifies PDF and RTF files
- Arhives
	- Zip files and jar files are stored into this folder.
- UNKNOWN
	- Files that do not match the above are copied over to this folder

Once sorted, the script keeps a log file to track where each file was copied.

## Duplicates Remover
This scripts recursively enumerates files in folder and removes and duplicates.
A log file of all deleted and ratained files is then saved in the folder in which the script is executed.

## Dowloads
Download the latest compiled version of the file sorter and duplicates remover:
- [Folder Sorter](../bin/files-sorter.exe)
- [Duplicates Remover](../bin/duplicate-remover.exe)
## Scripts/dependencies
- This is used for converting final script to exe:
	- [PS2EXE-GUI-Converter](https://gallery.technet.microsoft.com/scriptcenter/PS2EXE-GUI-Convert-e7cb69d5)
- This script below was slightly modified for sorter script to identify files by MIME type:
	- [Get-FileType](https://communary.net/2014/12/15/get-filetype/)
- The script above depends on the one below:
	- [Get-FileSignature](https://gallery.technet.microsoft.com/scriptcenter/Get-FileSignature-f5ae19f5)

	
