﻿#Add dependencies
#-----------------------------------------------------------------------Extras---------------------------------------------------------------
function Get-FileType {
    <#
        .SYNOPSIS
            Try to get the file type based on it's file signature.
        .DESCRIPTION
            This function uses Get-FileSignature by Boe Prox and a list of
            known file signatures to try to find the file type of a given file.
        .EXAMPLE
            Get-FileType c:\path\to\file.pdf
        .LINK
            https://gallery.technet.microsoft.com/scriptcenter/Get-FileSignature-f5ae19f5
        .NOTES
            Author: Øyvind Kallstad
            Date: 15.12.2014
            Version: 1.0
    #>
    [CmdletBinding()]
    param (
        # Path to file.
        [Parameter(Position = 0, ValueFromPipelineByPropertyName)]
        [string[]] $Path
    )

    begin {
        # description, hex signature, byte offset, byte limit
        $fileSignatures = @(
            @('JPEG','FFD8FF',0,3),
            @('PNG','89504E470D0A1A0A',0,8),
            @('GIF','474946383761',0,6),
            @('GIF','474946383961',0,6),
            @('Icon (ICO)','00000100',0,4),
            @('MP4','000000*66747970',0,8),
            @('MP4','336770',0,3),
            @('Windows/DOS executable file (EXE/COM/DLL/DRV/PIF/OCX/OLB/SCR/CPL ++)','4D5A',0,2),
            @('RAR','526172211A0700',0,7),
            @('RAR','526172211A070100',0,8),
            @('PDF','25504446',0,4),
            @('MP3','FFFB',0,2),
            @('MP3','494433',0,3),
            @('ISO','4344303031','0x8001',5),
            @('ISO','4344303031','0x8801',5),
            @('ISO','4344303031','0x9001',5),
            @('Install Shield compressed file (CAB/HDR)','49536328',0,4),
            @('Microsoft cabinet file (CAB) / Powerpoint Packaged Presentation (PPZ) / Microsoft Access Snapshot Viewer file (SNP)','4D534346',0,4),
            @('Microsoft Windows Imaging Format file (WIM)','4D5357494D',0,5),
            @('Rich text format (RTF)','7B5C72746631',0,6),
            @('TrueType font file (TTF)','0001000000',0,5),
            @('LNK','4C00000001140200',0,8),
            @('Windows Help (HLP/GID)','4C4E0200',0,4),
            @('Windows Help (HLP/GID)','3F5F0300',0,4),
            @('Windows Help (HLP)','0000FFFFFFFF',0,6),
            @('Windows Registry (REG/SUD)','52454745444954',0,7),
            @('Windows Registry (REG)','FFFE',0,2),
            @('ZIP','504B0304',0,4),
            @('Microsoft security catalog file (CAT)','30',0,1),
            @('Windows memory dump (DMP)','5041474544554D50',0,8),
            @('Windows 64-bit memory dump (DMP)','5041474544553634',0,8),
            @('Windows minidump (DMP) / Windows heap dump (HDMP)','4D444D5093A7',0,6),
            @('Microsoft Compiled HTML Help (CHM/CHI)','49545346',0,4),
            @('WAV','52494646*57415645',0,12)
        )
    }

    process {
        foreach ($filePath in $Path) {
            if (Test-Path $Path) {
                foreach($signature in $fileSignatures) {
                    if($thisSig = Get-FileSignature -Path $filePath -HexFilter $signature[1] -ByteOffset $signature[2] -ByteLimit $signature[3]) {
                        Write-Output (,([PSCustomObject] [Ordered] @{
                            Path = $filePath
                            FileType = ($signature[0])
                            HexSignature = $thisSig.HexSignature
                            ASCIISignature = $thisSig.ASCIISignature
                            Extension = $thisSig.Extension
                        }))
                    }
                }
            }
            else {
                Write-Warning "$filePath not found!"
            }
        }
    }
}

function Get-FileSignature { 
    <#
        .SYNOPSIS
            Displays a file signature for specified file or files

        .DESCRIPTION
            Displays a file signature for specified file or files. Determined by getting the bytes of a file and
            looking at the number of bytes to return and where in the byte array to start.

        .PARAMETER Path
            The path to a file. Can be multiple files.

        .PARAMETER HexFilter
            A filter that can be used to find specific Hex Signatures. Allows "*" wildcard.

        .PARAMETER ByteLimit
            How many bytes of the file signature to return. 
            Default value is 2. (Display first 2 bytes)

        .PARAMETER ByteOffset
            Where in the byte array to start displaying the signature. 
            Default value is 0 (first byte)

        .NOTES
            Name: Get-FileSignature
            Author: Boe Prox
            Created: 3 Jan 2014
            Version History
                1.4 -- 19 Feb 2014
                    -Fixed bug where a ' (CHAR 39) in path would cause script to halt attempts at pulling signature
                1.3 -- 27 Jan 2014
                    -Fixed annoying bug with a non-existent filestream trying to be closed causing
                    everything in Process block to quit
                1.2 -- 14 Jan 2014
                    -Added HexFilter parameter
                1.1 -- 12 Jan 2014
                    -Incorporated modified version of Get-FileTimestamp function for 
                    ChangeTime attribute
                1.0 -- 3 Jan 2014
                    -Initial Version

        .OUTPUTS
            System.IO.FileInfo.Signature

        .LINK
            http://en.wikipedia.org/wiki/List_of_file_signatures

        .EXAMPLE
            Get-FileSignature -Path PSExec.Exe

            Name           : PSExec.Exe
            FullName       : C:\Users\joeuser\Desktop\PSExec.Exe
            HexSignature   : 4D5A
            ASCIISignature : MZ
            Length         : 381816

            Description
            -----------
            Displays the file signature for psexec.exe using default parameter values. In this case, 
            the signature shows the file to be an executable.

        .EXAMPLE
            Get-ChildItem | Get-FileSignature -HexFilter 4D5A | Format-Table

            Name          FullName                             HexSignature ASCIISignature  Length
            ----          --------                             ------------ --------------  ------
            handle.exe    C:\Users\joeuser\Desktop\handle.exe    4D5A         MZ              423288
            PortQry.exe   C:\Users\joeuser\Desktop\PortQry.exe   4D5A         MZ              143360
            procexp.exe   C:\Users\joeuser\Desktop\procexp.exe   4D5A         MZ             4754224
            Procmon.exe   C:\Users\joeuser\Desktop\Procmon.exe   4D5A         MZ             2474608
            Procmon64.txt C:\Users\joeuser\Desktop\Procmon64.txt 4D5A         MZ             1259376
            PsExec.exe    C:\Users\joeuser\Desktop\PsExec.exe    4D5A         MZ              381816
            pskill.exe    C:\Users\joeuser\Desktop\pskill.exe    4D5A         MZ              468592
            Tcpview.exe   C:\Users\joeuser\Desktop\Tcpview.exe   4D5A         MZ              299896

            Description
            -----------
            Returns only files which match a specified signature (4D5A) for executables"

        .EXAMPLE
            Get-FileSignature -Path Procmon64.txt

            Name           : Procmon64.txt
            FullName       : C:\Users\joeuser\Desktop\Procmon64.txt
            HexSignature   : 4D5A
            ASCIISignature : MZ
            Length         : 1259376

            Description
            -----------
            Displays the file signature for Procmon64.txt using default parameter values. In this case, 
            the signature shows the file to be an executable even though it has a .txt extension.

        .EXAMPLE
            Get-FileSignature -Path OneNote2003.iso -ByteLimit 5 -ByteOffset 0x8001

            Name           : OneNote2003.iso
            FullName       : C:\Users\joeuser\Desktop\OneNote2003.iso
            HexSignature   : 4344 3030 31
            ASCIISignature : CD001
            Length         : 89960448

            Description
            -----------
            This returns the proper file signature for an ISO file by using a specified ByteOffset of 0x8001
            and the ByteLimit of 5.

    #>
    #Requires -Version 3.0
    [CmdletBinding()]
    Param(
       [Parameter(Position=0,Mandatory=$true, ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$True)]
       [Alias("FullName")]
       [string[]]$Path,
       [parameter()]
       [Alias('Filter')]
       [string]$HexFilter = "*",
       [parameter()]
       [int]$ByteLimit = 2,
       [parameter()]
       [Alias('OffSet')]
       [int]$ByteOffset = 0
    )
    Begin {
        #Determine how many bytes to return if using the $ByteOffset
        $TotalBytes = $ByteLimit + $ByteOffset

        #Clean up filter so we can perform a regex match
        #Also remove any spaces so we can make it easier to match
        [regex]$pattern = ($HexFilter -replace '\*','.*') -replace '\s',''

        #region Helper Functions
        Write-Verbose "Loading Get-FileTimeStamp helper function"
        Function Get-FileTimeStamp {
            [cmdletbinding()]
            Param (
                [parameter(ValueFromPipeline = $True,Mandatory=$True)]
                [IO.FileStream]$FileStream
            )
            Process {
                $sig = @'
                    using System;
                    using System.Runtime.InteropServices; 
                    using Microsoft.Win32.SafeHandles; 
                    using System.Collections.Generic; 
                    using System.IO;
                    using System.Text; 
                    using System.Threading; 
                    using System.Diagnostics; 
                    public class FileTimeStamp
                    {

                        struct IO_STATUS_BLOCK
                        {
                            internal uint status;
                            internal ulong information;
                        }
                        enum FILE_INFORMATION_CLASS
                        {
                            FileDirectoryInformation = 1,        // 1
                            FileFullDirectoryInformation,        // 2
                            FileBothDirectoryInformation,        // 3
                            FileBasicInformation,            // 4
                            FileStandardInformation,        // 5
                            FileInternalInformation,        // 6
                            FileEaInformation,            // 7
                            FileAccessInformation,            // 8
                            FileNameInformation,            // 9
                            FileRenameInformation,            // 10
                            FileLinkInformation,            // 11
                            FileNamesInformation,            // 12
                            FileDispositionInformation,        // 13
                            FilePositionInformation,        // 14
                            FileFullEaInformation,            // 15
                            FileModeInformation = 16,        // 16
                            FileAlignmentInformation,        // 17
                            FileAllInformation,            // 18
                            FileAllocationInformation,        // 19
                            FileEndOfFileInformation,        // 20
                            FileAlternateNameInformation,        // 21
                            FileStreamInformation,            // 22
                            FilePipeInformation,            // 23
                            FilePipeLocalInformation,        // 24
                            FilePipeRemoteInformation,        // 25
                            FileMailslotQueryInformation,        // 26
                            FileMailslotSetInformation,        // 27
                            FileCompressionInformation,        // 28
                            FileObjectIdInformation,        // 29
                            FileCompletionInformation,        // 30
                            FileMoveClusterInformation,        // 31
                            FileQuotaInformation,            // 32
                            FileReparsePointInformation,        // 33
                            FileNetworkOpenInformation,        // 34
                            FileAttributeTagInformation,        // 35
                            FileTrackingInformation,        // 36
                            FileIdBothDirectoryInformation,        // 37
                            FileIdFullDirectoryInformation,        // 38
                            FileValidDataLengthInformation,        // 39
                            FileShortNameInformation,        // 40
                            FileHardLinkInformation = 46        // 46    
                        }
                        [StructLayout(LayoutKind.Explicit)]
                        struct FILE_BASIC_INFORMATION
                        {
                            [FieldOffset(0)]
                            internal long CreationTime;
                            [FieldOffset(8)]
                            internal long LastAccessTime;
                            [FieldOffset(16)]
                            internal long LastWriteTime;
                            [FieldOffset(24)]
                            internal long ChangeTime;
                            [FieldOffset(32)]
                            internal ulong FileAttributes;
                        }
                        [DllImport("ntdll.dll", SetLastError = true)]
                        static extern IntPtr NtQueryInformationFile(SafeFileHandle fileHandle, ref IO_STATUS_BLOCK IoStatusBlock, IntPtr pInfoBlock, uint length, FILE_INFORMATION_CLASS fileInformation);

                        public static bool GetFourFileTimes(FileStream Stream,
                                out DateTime creationTime, out DateTime lastAccessTime, out DateTime lastWriteTime, out DateTime changeTime, out string errMsg)
                        {
                            bool brc = false;
                            creationTime = default(DateTime);
                            lastAccessTime = default(DateTime);
                            lastWriteTime = default(DateTime);
                            changeTime = default(DateTime);
                            errMsg = string.Empty;
                            IntPtr p_fbi = IntPtr.Zero;
                            try
                            {
                                FILE_BASIC_INFORMATION fbi = new FILE_BASIC_INFORMATION();
                                IO_STATUS_BLOCK iosb = new IO_STATUS_BLOCK();
                                using (Stream)
                                {
                                    p_fbi = Marshal.AllocHGlobal(Marshal.SizeOf(fbi));
                                    IntPtr iprc = NtQueryInformationFile(Stream.SafeFileHandle, ref iosb, p_fbi, (uint)Marshal.SizeOf(fbi), FILE_INFORMATION_CLASS.FileBasicInformation);
                                    brc = iprc == IntPtr.Zero && iosb.status == 0;
                                    if (brc)
                                    {
                                        brc = false;
                                        fbi = (FILE_BASIC_INFORMATION)Marshal.PtrToStructure(p_fbi, typeof(FILE_BASIC_INFORMATION));
                                        creationTime = DateTime.FromFileTime(fbi.CreationTime);
                                        lastAccessTime = DateTime.FromFileTime(fbi.LastAccessTime);
                                        lastWriteTime = DateTime.FromFileTime(fbi.LastWriteTime);
                                        changeTime = DateTime.FromFileTime(fbi.ChangeTime);
                                        brc = true;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                brc = false;
                                errMsg = ex.Message;
                            }
                            finally
                            {
                                if (p_fbi != IntPtr.Zero) { Marshal.FreeHGlobal(p_fbi); }
                            }
                            return brc;
                        }
                    }
'@
                #endregion Create signature from inline C# code

                #region Create Win32 API object
                Try {
                    [void][FileTimeStamp]
                } Catch {
                    Add-Type -TypeDefinition $sig
                }
                #endregion Create Win32 API object

                #region Create reference variables
                $creationTime = (Get-Date)
                $lastAccessTime = (Get-Date)
                $lastWriteTime = (Get-Date)
                $changeTime = (Get-Date)
                $errorMsg = $null
                #endregion Create reference variables

                #region Get file timestamps
                $return = [FileTimeStamp]::GetFourFileTimes($filestream,
                    [ref]$creationTime,
                    [ref]$lastAccessTime,
                    [ref]$lastWriteTime,
                    [ref]$changeTime,
                    [ref]$errorMsg
                )
                #endregion Get file timestamps
                If ($return) {
                    [datetime]$changeTime
                } Else {Write-Warning ("{0}" -f $errorMsg)}
            }
        }
        #endregion Helper Functions
    }
    Process { 
        ForEach ($item in $Path) { 
            Write-Verbose "Item: $($Item)"
            Try {    
                Try {
                    $item = Get-Item -LiteralPath (Convert-Path -LiteralPath $item -ErrorAction Stop) -Force -ErrorAction Stop
                } Catch {
                    #Need to replace the ' with a ’ so the convert-path will not fail                
                    $item = Get-Item -LiteralPath (Convert-Path -LiteralPath ($item -replace [char]39,[char]8217)) -Force -ErrorAction Stop
                }
            } Catch {
                Write-Warning "$($item): $($_.Exception.Message)"
                Return
            }
            If (Test-Path -Path $item -Type Container) {
                Write-Warning ("Cannot find signature on directory: {0}" -f $item)
            } Else {
                Try {
                    Write-Verbose "Comparing byte length ($($item.Length)) to TotalBytes ($totalbytes)"
                    If ($Item.length -ge $TotalBytes) {
                        #Open a FileStream to the file; this will prevent other actions against file until it closes
                        $filestream = New-Object IO.FileStream($Item, [IO.FileMode]::Open, [IO.FileAccess]::Read)

                        #Determine starting point
                        [void]$filestream.Seek($ByteOffset, [IO.SeekOrigin]::Begin)

                        #Create Byte buffer to read into and then read bytes from starting point to pre-determined stopping point
                        $bytebuffer = New-Object "Byte[]" ($filestream.Length - ($filestream.Length - $ByteLimit))
                        [void]$filestream.Read($bytebuffer, 0, $bytebuffer.Length)

                        #Create string builder objects for hex and ascii display
                        $hexstringBuilder = New-Object Text.StringBuilder
                        $stringBuilder = New-Object Text.StringBuilder

                        #Begin converting bytes
                        For ($i=0;$i -lt $ByteLimit;$i++) {
                            If ($i%2) {
                                [void]$hexstringBuilder.Append(("{0:X}" -f $bytebuffer[$i]).PadLeft(2, "0"))
                            } Else {
                                If ($i -eq 0) {
                                    [void]$hexstringBuilder.Append(("{0:X}" -f $bytebuffer[$i]).PadLeft(2, "0"))
                                } Else {
                                    [void]$hexstringBuilder.Append(" ")
                                    [void]$hexstringBuilder.Append(("{0:X}" -f $bytebuffer[$i]).PadLeft(2, "0"))
                                }        
                            }
                            If ([char]::IsLetterOrDigit($bytebuffer[$i])) {
                                [void]$stringBuilder.Append([char]$bytebuffer[$i])
                            } Else {
                                [void]$stringBuilder.Append(".")
                            }
                        }
                        If (($hexstringBuilder.ToString() -replace '\s','') -match $pattern) {
                            $object = [pscustomobject]@{
                                Name = ($item -replace '.*\\(.*)','$1')
                                FullName = $item
                                HexSignature = $hexstringBuilder.ToString()
                                ASCIISignature = $stringBuilder.ToString()
                                Length = $item.length
                                Extension = $Item.fullname -replace '.*\.(.*)','$1'
                                ChangeTime = ($FileStream | Get-FileTimeStamp)
                            }
                            $object.pstypenames.insert(0,'System.IO.FileInfo.Signature')
                            Write-Output $object
                        }
                    } ElseIf ($Item.length -eq 0) {
                        ## This can cause a lot of unneeded noise in the console so I am leaving this commented out
                        #Write-Warning ("{0} has no data ({1} bytes)!" -f $item.name,$item.length)
                        Return
                    } Else {                                                
                        ## This can cause a lot of unneeded noise in the console so I am leaving this commented out
                        #Write-Warning ("{0} size ({1}) is smaller than required total bytes ({2})" -f $item.name,$item.length,$TotalBytes)
                        Return
                    }
                } Catch {
                    Write-Warning ("{0}: {1}" -f $item,$_.Exception.Message)
                    Return
                }

                #Close the file stream so the file is no longer locked by the process
                $FileStream.Close()
            }
        }        
    }
}
#-----------END -Extras -------


#A"
$sortdir = "Sorted"
$currentDir = Get-Location
$PicturesDir = "$($currentDir)\$($sortdir)"
if(-Not (Test-Path $PicturesDir)){
    New-Item $PicturesDir -itemtype directory | Out-Null
}
$picoutdir = "$($PicturesDir)\Pictures"
$videoutdir = "$($PicturesDir)\Videos"
$documentdir = "$($PicturesDir)\Documents"
$audiodir ="$($PicturesDir)\Audio Files"
$archivedir = "$($PicturesDir)\Archives"
$unknown = "$($PicturesDir)\Unknown"
$logFile = "$($PicturesDir)\log.txt"

$storeMethod = 1

Function Log {
    param(
        [Parameter(Mandatory=$true)][String]$msg
    )
    Add-Content $logFile $msg   
}

function getfileClass($type){
   $map = @{ `
          Picture  = @('JPEG', 'GIF', 'PNG'); `
          Audio    = @('MP3','WAV'); ` 
          Video    = @('MP4');  `
          Document = @('PDF','RTF'); `
          ISO      = @('ISO'); `
          Archive  = @('ZIP','RAR');`
   }
   #Iterate through to check if the key of corresponing type
   foreach($key in $map.Keys){
        if($map.$key.Contains($type)){
            return $key
        }
   }
   return "UNKNOWN"
}

function getPicDate($piclocation){
    [reflection.assembly]::LoadWithPartialName("System.Drawing")
    $pic = New-Object System.Drawing.Bitmap($piclocation)
    try{
        $bitearr = $pic.GetPropertyItem(36867).Value 
        $string = [System.Text.Encoding]::ASCII.GetString($bitearr) 
        $DateTime = [datetime]::ParseExact($string,"yyyy:MM:dd HH:mm:ss`0",$Null)
        return $DateTime
    }catch{
        return $Null
    }
}

function SortAllFiles($dir){
    $files = Get-ChildItem $dir
    if($dir -eq $PicturesDir){
        return
    }
    $i = 0
    foreach($file in $files){
        if((Get-Item $file.FullName) -is [System.IO.DirectoryInfo]){
            #It is a directory lets go inside
            SortAllFiles $file.FullName
        }else{
            #Check File Type -- If picture sort it
            $fileType = $(Get-FileType $file.FullName).FileType
            switch((getfileClass $fileType)){
                "Picture" { sortPicture $file.FullName $file.Name }
                "Video"   { sortVideo $file.FullName $file.Name}
                 Default  { AnyFileSort $file.FullName $file.Name $(getfileClass $fileType)}
            }
        }
        $i = $i + 1
        Write-Progress -Activity "Sorting Files" -Status "Progress:" -PercentComplete ($i/$files.count*100)
    }
    logFormat "------" "--------------"
    logFormat "Origin" "Destination"
    Log "Sorting files inside $dir"
    Log " "

}
function AnyFileSort($location,$name,$type){
    $dest = ""
    switch($type){
        "Document" {$dest = $documentdir}
        "Audio"    {$dest = $audiodir}
        "Video"    {$dest = $videoutdir}
        "Archive"  {$dest = $archivedir}
        Default    {$dest = $unknown}
    }
    if(-Not (Test-Path $dest)){  
       New-Item $dest -itemtype directory | Out-Null
    }
    storeFile $storeMethod $location $dest
    #Copy-Item $location -Destination $videoutdir
    #build out hash table
    logFormat $name "$($dest)\$name"
}

function sortVideo($location,$name){
    if(-Not (Test-Path $videoutdir)){
       
       New-Item $videoutdir -itemtype directory | Out-Null
    }
    storeFile $storeMethod $location $videoutdir
    #Copy-Item $location -Destination $videoutdir
    #build out hash table
    logFormat $name "$($videoutdir)\$name"
}

function logFormat($name,$destination){
    $out = @{$name = "$destination"}
    Log ($out | Out-String -Stream)[3]
}

function storeFile($method, $origin,$destination){
    switch($method){
    #0 = Move; 1 = Copy
      0  {Move-Item -Path $origin -Destination $destination}
      1  {Copy-Item $origin -Destination $destination}
    }
}

function sortPicture($location,$name){
    if(-Not (Test-Path $picoutdir)){
       New-Item $picoutdir -itemtype directory | Out-Null
    }
    $pictureDatetime = getPicDate $location
    if(-Not ($pictureDatetime -eq $Null) -And -Not ($pictureDatetime[1] -eq $Null)){
        $year = $pictureDatetime.Year
        $month = (Get-Culture).DateTimeFormat.GetAbbreviatedMonthName($pictureDatetime.Month)
        saveSortedPicture $year $month $name $location
    }else{
        saveSortedPicture "Unknown" "Unknown" $name $location
    }
}

function saveSortedPicture($year,$month,$filename,$fromLocation){
    #$basedir = Get-Location
    $basedir = $picoutdir
    $YearDir = "$($basedir)\$($year)"
    $MonthDir = "$($YearDir)\$($month)"
    $dest = "$($MonthDir)\$($filename)"
    if(Test-Path $YearDir){
       if(-Not (Test-Path $MonthDir)){
          New-Item $MonthDir -itemtype directory | Out-Null
       }
    }else{
       New-Item $YearDir -itemtype directory | Out-Null
       saveSortedPicture $year $month $filename $fromLocation
    }
    #Save picutre here
    logFormat $name $dest
    if(-Not (Test-Path $dest)){
        #Copy-Item $fromLocation -Destination $dest
        storeFile $storeMethod $fromLocation $dest 
    }
}

function Get-MimeType()
{
  param($extension = $null);
  $mimeType = $null;
  if ( $null -ne $extension )
  {
    $drive = Get-PSDrive HKCR -ErrorAction SilentlyContinue;
    if ( $null -eq $drive )
    {
      $drive = New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT
    }
    $mimeType = (Get-ItemProperty HKCR:$extension)."Content Type";
  }
  $mimeType;
}

function deleteOld($dir){
    $oldFiles = Get-ChildItem $dir | ? { $_.FullName -inotmatch $sortdir}
    foreach($file in $oldFiles){
        try{
            Remove-Item $file -Recurse
            Log "Deleted: $file"
        }catch{
            Log "Could not remove $file"
        }
    }
    $lastFile = Get-ChildItem $dir | ? { $_.FullName -inotmatch $sortdir}
    $lastFile | Remove-Item
    Log "Deleted: $lastFile"
    Log "-------------------Deleting old files..."
}

Write-Host "Sorting your files! This will inform you when complete!"
if(Test-Path $logFile){
    Remove-Item $logFile
}

$count = ((Get-ChildItem $currentDir  -Recurse -File | Measure-Object | %{$_.Count}) -as [int]) - 1

SortAllFiles $currentDir 
deleteOld $currentDir 

Write-Host "Complete! Read log file($($logFile)) to view list of sorted Files"

$sorted = ((Get-ChildItem $PicturesDir -Recurse -File | Measure-Object | %{$_.Count}) -as [int]) - 2

Log "$sorted successuffly sorted!"
Log "$count files contained in $currentDir"

$x = Get-Content -Path $logFile; Set-Content -Path $logFile -Value ($x[($x.Length-1)..0])

